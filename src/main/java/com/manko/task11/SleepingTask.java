package com.manko.task11;

import java.time.Duration;
import java.time.Instant;

public class SleepingTask implements Runnable {

    private int sleepingTime;

    public SleepingTask(int sleepingTime) {
        this.sleepingTime = sleepingTime;
    }

    @Override
    public void run() {
        Instant start = Instant.now();
        try {
            Thread.sleep(sleepingTime * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Instant end = Instant.now();
        Duration totalTime = Duration.between(start, end);
        System.out.println(totalTime.toMillis() / 1000 + " seconds");
    }
}
