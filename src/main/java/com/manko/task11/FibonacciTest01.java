package com.manko.task11;

public class FibonacciTest01 {

    public static void main(String[] args) {
        for (int i = 5; i < 20; i+=3) {
            System.out.println("i = " + i);
            Thread newThread = new Thread(new FibonacciClass(i));
            newThread.start();
        }
    }
}
