package com.manko.task11;

import java.util.concurrent.*;

public class FibonacciTest02 {

    public static void main(String[] args) {
//        test1();
//        test2();
//        test3();
        test4();

    }

    // using SingleThreadExecutor
    private static void test1() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        doTask(executorService);
    }

    // using FixedThreadPool
    private static void test2() {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        doTask(executorService);
    }

    // using CachedThreadPool
    private static void test3() {
        ExecutorService executorService = Executors.newCachedThreadPool();
        doTask(executorService);
    }

    // using SingleThreadScheduledExecutor
    private static void test4() {
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        for (int i = 5; i < 20; i+=3) {
            System.out.println("i = " + i);
            service.schedule(new FibonacciClass(i), 3, TimeUnit.SECONDS);
        }
        service.shutdown();
    }

    private static void doTask(ExecutorService service) {
        service.submit(() -> {
            for (int i = 5; i < 20; i+=3) {
                System.out.println("i = " + i);
                Thread newThread = new Thread(new FibonacciClass(i));
                newThread.start();
            }});
        service.shutdown();
    }
}
