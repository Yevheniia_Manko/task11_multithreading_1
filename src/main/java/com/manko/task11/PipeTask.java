package com.manko.task11;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class PipeTask {

    public static void main(String[] args) throws Exception {

        try (PipedInputStream pipeIn = new PipedInputStream();
        PipedOutputStream pipeOut = new PipedOutputStream()) {
            pipeIn.connect(pipeOut);
            Runnable taskWrite = new Runnable() {
                @Override
                public void run() {
                    for (int i = 65; i < 91; i++) {
                        try {
                            System.out.println(Thread.currentThread().getName());
                            pipeOut.write(i);
                            Thread.sleep(200);
                        } catch (IOException | InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println(Thread.currentThread().getName() + " finished.");
                }
            };
            Runnable taskRead = new Runnable() {
                @Override
                public void run() {
                    for (int i = 65; i < 91; i++) {
                        try {
                            System.out.println(Thread.currentThread().getName());
                            System.out.println((char) pipeIn.read());
                            Thread.sleep(200);
                        } catch (IOException | InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println(Thread.currentThread().getName() + " finished.");
                }
            };
            Thread threadOut = new Thread(taskWrite, "Thread TaskWrite");
            Thread threadIn = new Thread(taskRead, "Thread TaskRead");
            threadOut.start();
            threadIn.start();
            threadOut.join();
            threadIn.join();
        }
    }
}
