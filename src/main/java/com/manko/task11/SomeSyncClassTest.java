package com.manko.task11;

public class SomeSyncClassTest {

    public static void main(String[] args) {
        // uncomment for testing asynchronized methods
        SomeAsyncClass someClass = new SomeAsyncClass();
        // uncomment for testing synchronized methods
//        SomeSyncClass someClass = new SomeSyncClass();

        Thread t1 = new Thread(someClass::methodOne);
        Thread t2 = new Thread(someClass::methodTwo);
        Thread t3 = new Thread(someClass::methodThree);
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
