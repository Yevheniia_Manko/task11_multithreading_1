package com.manko.task11;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class FibonacciCallableClass implements Callable<Integer> {

    private int count;

    public FibonacciCallableClass(int count) {
        this.count = count;
    }

    private int getFibonacciNumber(int number) {
        if (number == 0) {
            return 0;
        }
        if (number == 1) {
            return 1;
        }
        else {
            return getFibonacciNumber(number - 1) + getFibonacciNumber(number - 2);
        }
    }

    @Override
    public Integer call() throws Exception {
        int sum = 0;
        for (int i = 0; i < count; i++) {
            sum += getFibonacciNumber(i);
        }
        return sum;
    }

    public static void main(String[] args) {
        Callable<Integer> fibonacciCallableClass = new FibonacciCallableClass(9);
        FutureTask<Integer> futureTask = new FutureTask<>(fibonacciCallableClass);
        new Thread(futureTask).start();
        try {
            System.out.println(futureTask.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
