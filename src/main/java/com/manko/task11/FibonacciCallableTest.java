package com.manko.task11;

import java.util.concurrent.*;

public class FibonacciCallableTest {

    public static void main(String[] args) {
//        test1();
        test2();
    }

    private static void test1() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Callable<Integer> callable = new FibonacciCallableClass(9);
        try {
            System.out.println(executorService.submit(callable).get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
    }

    private static void test2() {
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 2; i < 10; i++) {
            Callable<Integer> callable = new FibonacciCallableClass(i);
            try {
                System.out.println(executorService.submit(callable).get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        executorService.shutdown();
    }
}
