package com.manko.task11;

public class SomeSyncClass {

    private final Object lock = new Object();

    public void methodOne() {
        synchronized (lock) {
            for (int i = 0; i < 5; i++) {
                System.out.println("MethodOne number = " + i + " " + Thread.currentThread().getName());
            }
        }
        System.out.println(Thread.currentThread().getName() + " finished.");
    }

    public void methodTwo() {
        synchronized (lock) {
            for (int i = 10; i < 15; i++) {
                System.out.println("MethodTwo number = " + i + " " + Thread.currentThread().getName());
            }
        }
        System.out.println(Thread.currentThread().getName() + " finished.");
    }

    public void methodThree() {
        synchronized (lock) {
            for (int i = 20; i < 25; i++) {
                System.out.println("MethodThree number = " + i + " " + Thread.currentThread().getName());
            }
        }
        System.out.println(Thread.currentThread().getName() + " finished.");
    }
}
