package com.manko.task11;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SleepingTaskTest {

    public static void main(String[] args) {
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(5);
        for (int i = 0; i < 10; i++) {
            int delay = (int) (Math.random() * 10 + 1);
            System.out.println("delay = " + delay);
            Thread thread = new Thread(new SleepingTask(delay));
            executorService.schedule(thread, 5, TimeUnit.SECONDS);
        }
        executorService.shutdown();
    }
}
