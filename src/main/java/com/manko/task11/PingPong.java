package com.manko.task11;

import java.time.LocalTime;

public class PingPong {

    private static Object lock = new Object();

    public void show() {
        Thread t1 = new Thread(() -> {synchronized (lock) {
            for (int i = 0; i < 10; i++) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(i + " Hello! I'm " + Thread.currentThread().getName());
                lock.notify();
            }
        }}, "Thread1");
        Thread t2 = new Thread(() -> {synchronized (lock) {
            for (int i = 0; i < 10; i++) {
                lock.notify();
                try {
                   lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(i + " Hi! I'm " + Thread.currentThread().getName());
            }
        }}, "Thread2");
        System.out.println(LocalTime.now());
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(LocalTime.now() + " I'm thread " + Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        PingPong pingPong = new PingPong();
        pingPong.show();
    }
}
