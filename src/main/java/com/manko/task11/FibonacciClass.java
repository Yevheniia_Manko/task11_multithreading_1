package com.manko.task11;

public class FibonacciClass implements Runnable {

    private int count;

    public FibonacciClass(int count) {
        this.count = count;
    }

    private int getFibonacciNumber(int number) {
        if (number == 0) {
            return 0;
        }
        if (number == 1) {
            return 1;
        }
        else {
            return getFibonacciNumber(number - 1) + getFibonacciNumber(number - 2);
        }
    }

    @Override
    public void run() {
        for (int i = 0; i < count; i++) {
            System.out.print(getFibonacciNumber(i) + " ");
        }
        System.out.println();
    }
}
